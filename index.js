const getCube = 2 ** 3;
console.log(`The cube of 2 is ${getCube}`);

const address = ['258', 'Washington Ave NW', 'California', '90011'];
const [addressNumber, city, state, zipCode ] = address;
console.log(`I live at ${addressNumber} ${city}, ${state} ${zipCode}`);

const animal = {
	name: 'Lolong',
	type: 'saltwater crocodile',
	weight: '1075 kgs',
	length: '20 ft 3 in'
}
const {name, type, weight, length} = animal;
console.log(`${name} was a ${type}. He weighed at ${weight} with a measurement of ${length}.`);

const numbers = [1, 2, 3, 4, 5];
numbers.forEach((numbers) => {
	console.log(numbers);
})

const reduceNumber = numbers.reduce((accumulator, currentValue) => accumulator + currentValue); 
console.log(reduceNumber);

class Dog {
	constructor(name, age, breed){
		this.name = name;
		this.age = age;
		this.breed = breed;
	}
}

const myDog = new Dog('Frankie', 5, 'Miniature Daschund');
console.log(myDog);
